// tag::HeaderFullDisplayName[]
= ILTSN_UserGroup_used_T.dtsx
// end::HeaderFullDisplayName[]

== GeneralList

// tag::GeneralList[]
[cols="1,4l"]
|===

|PackageCreationDate
|2020-03-06 13:30:16

|PackageCreatorComputerName
|ACPROD

|PackageCreatorName
|ACPROD\dsorokin

|PackageDTSID
|E7268605-873B-477A-BFCF-CCE147F69097

|PackageLastModifiedProductVersion
|13.0.4604.0

|PackageLocaleID
|1031

|PackageProtectionLevel
|0

|PackageProtectionLevelName
|DontSaveSensitive

|PackageVersionGUID
|7B141EBC-5B58-4181-9188-C3FA6F7474AA

|ProjectPath
|D:\Repos\Azure\aisberg\ServiceNow\SSIS\SSIS_ServiceNow

|===

// end::GeneralList[]

== PumlPackageControlFlows

// tag::PumlPackageControlFlows[]
[plantuml, ssis-{docname}, svg, subs=macros]
....
@startuml

package "ILTSN_UserGroup_used_T.dtsx" {
  component "Persist VIEWs" as packagebackslashpersistblankviews <<STOCK:SEQUENCE>>  [[../iltsn_usergroup_used_t.html#task-packagebackslashpersistblankviews]] {
    component "Persist UserGroup_used" as packagebackslashpersistblankviewsbackslashpersistblankusergroupunderlineused <<Microsoft.ExecuteSQLTask>>  [[../iltsn_usergroup_used_t.html#task-packagebackslashpersistblankviewsbackslashpersistblankusergroupunderlineused]]
}





skinparam component {
    backgroundColor<<Microsoft.ASExecuteDDLTask>>   Aqua
    backgroundColor<<Microsoft.DTSProcessingTask>>  DeepSkyBlue
    backgroundColor<<Microsoft.ExecutePackageTask>> Yellow
    backgroundColor<<Microsoft.ExecuteSQLTask>>     Khaki
    backgroundColor<<Microsoft.ExpressionTask>>     Pink
    backgroundColor<<Microsoft.Pipeline>>           LightCyan
    backgroundColor<<Microsoft.ScriptTask>>         LightGreen
    backgroundColor<<STOCK:FOREACHLOOP>>            LightGray
    backgroundColor<<STOCK:SEQUENCE>>               White
}



footer The diagram is interactive and contains links.

@enduml

....
// end::PumlPackageControlFlows[]

== ConnectionList

// tag::ConnectionList[]
[#connection-config]
=== config

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|DA81B085-5776-46ED-B015-52CCDC0A8B47

|ExpressionValue
|@[$Project::ACR_ENV]

|===

[#connection-target]
=== Target

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|60D6E7CB-3E9C-4870-BF6B-5D4580DAEA71

|===

// end::ConnectionList[]

== ProjectConnectionList

// tag::ProjectConnectionList[]

// end::ProjectConnectionList[]

== ParameterList

// tag::ParameterList[]

// end::ParameterList[]

== VariableList

// tag::VariableList[]

// end::VariableList[]

== TaskList

// tag::TaskList[]
[#task-packagebackslashpersistblankviews]
=== Package{backslash}Persist VIEWs

[cols="1,4l"]
|===

|Task Path
|Package\Persist VIEWs

|Task Name
|Persist VIEWs

|Task Type
|STOCK:SEQUENCE

|Is Disabled
|False

|===

[#task-packagebackslashpersistblankviewsbackslashpersistblankusergroupunderlineused]
=== Package{backslash}Persist VIEWs{backslash}Persist UserGroup_used

[cols="1,4l"]
|===

|Task Path
|Package\Persist VIEWs\Persist UserGroup_used

|Task Name
|Persist UserGroup_used

|Task Type
|Microsoft.ExecuteSQLTask

|Is Disabled
|False

|Sql Connection
|60D6E7CB-3E9C-4870-BF6B-5D4580DAEA71

|Sql Package Connection
a| <<connection-target>>

|Sql Statement
a|[%collapsible]
=======
[source,sql,numbered]
----
EXEC [CFG].[PERSIST_VIEW]
	@PersistView = '[ILTSN].[UserGroup_used]', 
	@PersistTable = '[ILTSN].[UserGroup_used_T]', 
	@PreSQL = '', 
	@PostSQL = '', 
	@DropIndexes = '', 
	@CreateIndexes = '', 
	@sSnapshotIDs = '',
    @LogPaketName = 'ILTSN_UserGroup_used_T',
	@sDWHIDField = 'HistId',
	@sDateFromField = 'HistDateFrom',
	@sDateToFiels = 'HistDateTo'
----
=======


|===

// end::TaskList[]

== PackageDTSID

// tag::PackageDTSID[]
E7268605-873B-477A-BFCF-CCE147F69097
// end::PackageDTSID[]

== PackageCreationDate

// tag::PackageCreationDate[]
Mar  6 2020  1:30PM
// end::PackageCreationDate[]

== PackageCreatorComputerName

// tag::PackageCreatorComputerName[]
ACPROD
// end::PackageCreatorComputerName[]

== PackageCreatorName

// tag::PackageCreatorName[]
ACPROD\dsorokin
// end::PackageCreatorName[]

== PackageDescription

// tag::PackageDescription[]

// end::PackageDescription[]

== PackageLastModifiedProductVersion

// tag::PackageLastModifiedProductVersion[]
13.0.4604.0
// end::PackageLastModifiedProductVersion[]

== PackageLocaleID

// tag::PackageLocaleID[]
1031
// end::PackageLocaleID[]

== PackageObjectName

// tag::PackageObjectName[]
ILTSN_UserGroup_used_T
// end::PackageObjectName[]

== PackagePath

// tag::PackagePath[]
D:\Repos\Azure\aisberg\ServiceNow\SSIS\SSIS_ServiceNow\ILTSN_UserGroup_used_T.dtsx
// end::PackagePath[]

== PackageProtectionLevel

// tag::PackageProtectionLevel[]
0
// end::PackageProtectionLevel[]

== PackageProtectionLevelName

// tag::PackageProtectionLevelName[]
DontSaveSensitive
// end::PackageProtectionLevelName[]

== PackageVersionGUID

// tag::PackageVersionGUID[]
7B141EBC-5B58-4181-9188-C3FA6F7474AA
// end::PackageVersionGUID[]

== ProjectPath

// tag::ProjectPath[]
D:\Repos\Azure\aisberg\ServiceNow\SSIS\SSIS_ServiceNow
// end::ProjectPath[]

