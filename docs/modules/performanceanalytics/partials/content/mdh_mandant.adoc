// tag::HeaderFullDisplayName[]
= mdh_Mandant.dtsx
// end::HeaderFullDisplayName[]

== GeneralList

// tag::GeneralList[]
[cols="1,4l"]
|===

|PackageCreationDate
|2020-07-29 17:15:40

|PackageCreatorComputerName
|ACPROD

|PackageCreatorName
|ACPROD\dsorokin

|PackageDTSID
|4506A1CC-6935-4DFE-9D5B-D31FA532FFB6

|PackageLastModifiedProductVersion
|13.0.4604.0

|PackageLocaleID
|1031

|PackageProtectionLevel
|0

|PackageProtectionLevelName
|DontSaveSensitive

|PackageVersionGUID
|B72D4461-C629-4D7B-81C9-479AAADE7BFA

|ProjectPath
|D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics

|===

// end::GeneralList[]

== PumlPackageControlFlows

// tag::PumlPackageControlFlows[]
[plantuml, ssis-{docname}, svg, subs=macros]
....
@startuml

package "mdh_Mandant.dtsx" {
  component "Run procedure CFG_HIST_mdh_Mandant" as packagebackslashrunblankprocedureblankcfgunderlinehistunderlinemdhunderlinemandant <<Microsoft.ExecuteSQLTask>>  [[../mdh_mandant.html#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlinemdhunderlinemandant]]
}





skinparam component {
    backgroundColor<<Microsoft.ASExecuteDDLTask>>   Aqua
    backgroundColor<<Microsoft.DTSProcessingTask>>  DeepSkyBlue
    backgroundColor<<Microsoft.ExecutePackageTask>> Yellow
    backgroundColor<<Microsoft.ExecuteSQLTask>>     Khaki
    backgroundColor<<Microsoft.ExpressionTask>>     Pink
    backgroundColor<<Microsoft.Pipeline>>           LightCyan
    backgroundColor<<Microsoft.ScriptTask>>         LightGreen
    backgroundColor<<STOCK:FOREACHLOOP>>            LightGray
    backgroundColor<<STOCK:SEQUENCE>>               White
}



footer The diagram is interactive and contains links.

@enduml

....
// end::PumlPackageControlFlows[]

== ConnectionList

// tag::ConnectionList[]
[#connection-config]
=== config

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|D9F440EF-F99F-44A4-8F7F-9599E5BE2371

|ExpressionValue
|@[$Project::Config]

|===

[#connection-target]
=== Target

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|8A7FEB60-2869-432A-A98C-43A5D773E60A

|ExpressionValue
|@[$Project::Target]

|===

// end::ConnectionList[]

== ProjectConnectionList

// tag::ProjectConnectionList[]

// end::ProjectConnectionList[]

== ParameterList

// tag::ParameterList[]

// end::ParameterList[]

== VariableList

// tag::VariableList[]

// end::VariableList[]

== TaskList

// tag::TaskList[]
[#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlinemdhunderlinemandant]
=== Package{backslash}Run procedure CFG_HIST_mdh_Mandant

[cols="1,4l"]
|===

|Task Path
|Package\Run procedure CFG_HIST_mdh_Mandant

|Task Name
|Run procedure CFG_HIST_mdh_Mandant

|Task Type
|Microsoft.ExecuteSQLTask

|Is Disabled
|False

|Sql Connection
|8A7FEB60-2869-432A-A98C-43A5D773E60A

|Sql Package Connection
a| <<connection-target>>

|Sql Statement
a|[%collapsible]
=======
[source,sql,numbered]
----
EXEC [CFG].[HIST_mdh_Mandant]
----
=======


|===

// end::TaskList[]

== PackageDTSID

// tag::PackageDTSID[]
4506A1CC-6935-4DFE-9D5B-D31FA532FFB6
// end::PackageDTSID[]

== PackageCreationDate

// tag::PackageCreationDate[]
Jul 29 2020  5:15PM
// end::PackageCreationDate[]

== PackageCreatorComputerName

// tag::PackageCreatorComputerName[]
ACPROD
// end::PackageCreatorComputerName[]

== PackageCreatorName

// tag::PackageCreatorName[]
ACPROD\dsorokin
// end::PackageCreatorName[]

== PackageDescription

// tag::PackageDescription[]

// end::PackageDescription[]

== PackageLastModifiedProductVersion

// tag::PackageLastModifiedProductVersion[]
13.0.4604.0
// end::PackageLastModifiedProductVersion[]

== PackageLocaleID

// tag::PackageLocaleID[]
1031
// end::PackageLocaleID[]

== PackageObjectName

// tag::PackageObjectName[]
mdh_Mandant
// end::PackageObjectName[]

== PackagePath

// tag::PackagePath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics\mdh_Mandant.dtsx
// end::PackagePath[]

== PackageProtectionLevel

// tag::PackageProtectionLevel[]
0
// end::PackageProtectionLevel[]

== PackageProtectionLevelName

// tag::PackageProtectionLevelName[]
DontSaveSensitive
// end::PackageProtectionLevelName[]

== PackageVersionGUID

// tag::PackageVersionGUID[]
B72D4461-C629-4D7B-81C9-479AAADE7BFA
// end::PackageVersionGUID[]

== ProjectPath

// tag::ProjectPath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics
// end::ProjectPath[]

