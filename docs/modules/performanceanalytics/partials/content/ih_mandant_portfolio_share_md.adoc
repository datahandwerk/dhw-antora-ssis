// tag::HeaderFullDisplayName[]
= ih_Mandant_Portfolio_share_md.dtsx
// end::HeaderFullDisplayName[]

== GeneralList

// tag::GeneralList[]
[cols="1,4l"]
|===

|PackageCreationDate
|2020-07-29 17:15:27

|PackageCreatorComputerName
|ACPROD

|PackageCreatorName
|ACPROD\dsorokin

|PackageDTSID
|25417F1C-0D25-4253-AB3B-CB9BD0AD3279

|PackageLastModifiedProductVersion
|13.0.4604.0

|PackageLocaleID
|1031

|PackageProtectionLevel
|0

|PackageProtectionLevelName
|DontSaveSensitive

|PackageVersionGUID
|37F164E3-01CE-41E6-82FB-B83C4C3109AB

|ProjectPath
|D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics

|===

// end::GeneralList[]

== PumlPackageControlFlows

// tag::PumlPackageControlFlows[]
[plantuml, ssis-{docname}, svg, subs=macros]
....
@startuml

package "ih_Mandant_Portfolio_share_md.dtsx" {
  component "Run procedure CFG_HIST_ih_Mandant_Portfolio_share_md" as packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlineshareunderlinemd <<Microsoft.ExecuteSQLTask>>  [[../ih_mandant_portfolio_share_md.html#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlineshareunderlinemd]]
}





skinparam component {
    backgroundColor<<Microsoft.ASExecuteDDLTask>>   Aqua
    backgroundColor<<Microsoft.DTSProcessingTask>>  DeepSkyBlue
    backgroundColor<<Microsoft.ExecutePackageTask>> Yellow
    backgroundColor<<Microsoft.ExecuteSQLTask>>     Khaki
    backgroundColor<<Microsoft.ExpressionTask>>     Pink
    backgroundColor<<Microsoft.Pipeline>>           LightCyan
    backgroundColor<<Microsoft.ScriptTask>>         LightGreen
    backgroundColor<<STOCK:FOREACHLOOP>>            LightGray
    backgroundColor<<STOCK:SEQUENCE>>               White
}



footer The diagram is interactive and contains links.

@enduml

....
// end::PumlPackageControlFlows[]

== ConnectionList

// tag::ConnectionList[]
[#connection-config]
=== config

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|81E3EFB1-2632-4991-B22A-10E00DCD27F1

|ExpressionValue
|@[$Project::Config]

|===

[#connection-target]
=== Target

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|07099CF1-3484-4ABE-AE03-92EC5969301A

|ExpressionValue
|@[$Project::Target]

|===

// end::ConnectionList[]

== ProjectConnectionList

// tag::ProjectConnectionList[]

// end::ProjectConnectionList[]

== ParameterList

// tag::ParameterList[]

// end::ParameterList[]

== VariableList

// tag::VariableList[]

// end::VariableList[]

== TaskList

// tag::TaskList[]
[#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlineshareunderlinemd]
=== Package{backslash}Run procedure CFG_HIST_ih_Mandant_Portfolio_share_md

[cols="1,4l"]
|===

|Task Path
|Package\Run procedure CFG_HIST_ih_Mandant_Portfolio_share_md

|Task Name
|Run procedure CFG_HIST_ih_Mandant_Portfolio_share_md

|Task Type
|Microsoft.ExecuteSQLTask

|Is Disabled
|False

|Sql Connection
|07099CF1-3484-4ABE-AE03-92EC5969301A

|Sql Package Connection
a| <<connection-target>>

|Sql Statement
a|[%collapsible]
=======
[source,sql,numbered]
----
EXEC [CFG].[HIST_ih_Mandant_Portfolio_share_md]
----
=======


|===

// end::TaskList[]

== PackageDTSID

// tag::PackageDTSID[]
25417F1C-0D25-4253-AB3B-CB9BD0AD3279
// end::PackageDTSID[]

== PackageCreationDate

// tag::PackageCreationDate[]
Jul 29 2020  5:15PM
// end::PackageCreationDate[]

== PackageCreatorComputerName

// tag::PackageCreatorComputerName[]
ACPROD
// end::PackageCreatorComputerName[]

== PackageCreatorName

// tag::PackageCreatorName[]
ACPROD\dsorokin
// end::PackageCreatorName[]

== PackageDescription

// tag::PackageDescription[]

// end::PackageDescription[]

== PackageLastModifiedProductVersion

// tag::PackageLastModifiedProductVersion[]
13.0.4604.0
// end::PackageLastModifiedProductVersion[]

== PackageLocaleID

// tag::PackageLocaleID[]
1031
// end::PackageLocaleID[]

== PackageObjectName

// tag::PackageObjectName[]
ih_Mandant_Portfolio_share_md
// end::PackageObjectName[]

== PackagePath

// tag::PackagePath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics\ih_Mandant_Portfolio_share_md.dtsx
// end::PackagePath[]

== PackageProtectionLevel

// tag::PackageProtectionLevel[]
0
// end::PackageProtectionLevel[]

== PackageProtectionLevelName

// tag::PackageProtectionLevelName[]
DontSaveSensitive
// end::PackageProtectionLevelName[]

== PackageVersionGUID

// tag::PackageVersionGUID[]
37F164E3-01CE-41E6-82FB-B83C4C3109AB
// end::PackageVersionGUID[]

== ProjectPath

// tag::ProjectPath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics
// end::ProjectPath[]

