// tag::HeaderFullDisplayName[]
= ih_Mandant_Portfolio_Gewicht_AT.dtsx
// end::HeaderFullDisplayName[]

== GeneralList

// tag::GeneralList[]
[cols="1,4l"]
|===

|PackageCreationDate
|2020-07-29 17:15:26

|PackageCreatorComputerName
|ACPROD

|PackageCreatorName
|ACPROD\dsorokin

|PackageDTSID
|FF8AE91C-95CB-4B80-92DF-73436A09AA42

|PackageLastModifiedProductVersion
|13.0.4604.0

|PackageLocaleID
|1031

|PackageProtectionLevel
|0

|PackageProtectionLevelName
|DontSaveSensitive

|PackageVersionGUID
|66BF1536-F741-4F07-B417-CE0F5E7827CE

|ProjectPath
|D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics

|===

// end::GeneralList[]

== PumlPackageControlFlows

// tag::PumlPackageControlFlows[]
[plantuml, ssis-{docname}, svg, subs=macros]
....
@startuml

package "ih_Mandant_Portfolio_Gewicht_AT.dtsx" {
  component "Run procedure CFG_HIST_ih_Mandant_Portfolio_Gewicht_AT" as packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlinegewichtunderlineat <<Microsoft.ExecuteSQLTask>>  [[../ih_mandant_portfolio_gewicht_at.html#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlinegewichtunderlineat]]
}





skinparam component {
    backgroundColor<<Microsoft.ASExecuteDDLTask>>   Aqua
    backgroundColor<<Microsoft.DTSProcessingTask>>  DeepSkyBlue
    backgroundColor<<Microsoft.ExecutePackageTask>> Yellow
    backgroundColor<<Microsoft.ExecuteSQLTask>>     Khaki
    backgroundColor<<Microsoft.ExpressionTask>>     Pink
    backgroundColor<<Microsoft.Pipeline>>           LightCyan
    backgroundColor<<Microsoft.ScriptTask>>         LightGreen
    backgroundColor<<STOCK:FOREACHLOOP>>            LightGray
    backgroundColor<<STOCK:SEQUENCE>>               White
}



footer The diagram is interactive and contains links.

@enduml

....
// end::PumlPackageControlFlows[]

== ConnectionList

// tag::ConnectionList[]
[#connection-config]
=== config

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|5456EC7B-DB6C-40E4-8FEA-B22CAE872E87

|ExpressionValue
|@[$Project::Config]

|===

[#connection-target]
=== Target

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|1F695B57-D5BC-4919-9FF8-C5E76990AC8F

|ExpressionValue
|@[$Project::Target]

|===

// end::ConnectionList[]

== ProjectConnectionList

// tag::ProjectConnectionList[]

// end::ProjectConnectionList[]

== ParameterList

// tag::ParameterList[]

// end::ParameterList[]

== VariableList

// tag::VariableList[]

// end::VariableList[]

== TaskList

// tag::TaskList[]
[#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlineihunderlinemandantunderlineportfoliounderlinegewichtunderlineat]
=== Package{backslash}Run procedure CFG_HIST_ih_Mandant_Portfolio_Gewicht_AT

[cols="1,4l"]
|===

|Task Path
|Package\Run procedure CFG_HIST_ih_Mandant_Portfolio_Gewicht_AT

|Task Name
|Run procedure CFG_HIST_ih_Mandant_Portfolio_Gewicht_AT

|Task Type
|Microsoft.ExecuteSQLTask

|Is Disabled
|False

|Sql Connection
|1F695B57-D5BC-4919-9FF8-C5E76990AC8F

|Sql Package Connection
a| <<connection-target>>

|Sql Statement
a|[%collapsible]
=======
[source,sql,numbered]
----
EXEC [CFG].[HIST_ih_Mandant_Portfolio_Gewicht_AT]
----
=======


|===

// end::TaskList[]

== PackageDTSID

// tag::PackageDTSID[]
FF8AE91C-95CB-4B80-92DF-73436A09AA42
// end::PackageDTSID[]

== PackageCreationDate

// tag::PackageCreationDate[]
Jul 29 2020  5:15PM
// end::PackageCreationDate[]

== PackageCreatorComputerName

// tag::PackageCreatorComputerName[]
ACPROD
// end::PackageCreatorComputerName[]

== PackageCreatorName

// tag::PackageCreatorName[]
ACPROD\dsorokin
// end::PackageCreatorName[]

== PackageDescription

// tag::PackageDescription[]

// end::PackageDescription[]

== PackageLastModifiedProductVersion

// tag::PackageLastModifiedProductVersion[]
13.0.4604.0
// end::PackageLastModifiedProductVersion[]

== PackageLocaleID

// tag::PackageLocaleID[]
1031
// end::PackageLocaleID[]

== PackageObjectName

// tag::PackageObjectName[]
ih_Mandant_Portfolio_Gewicht_AT
// end::PackageObjectName[]

== PackagePath

// tag::PackagePath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics\ih_Mandant_Portfolio_Gewicht_AT.dtsx
// end::PackagePath[]

== PackageProtectionLevel

// tag::PackageProtectionLevel[]
0
// end::PackageProtectionLevel[]

== PackageProtectionLevelName

// tag::PackageProtectionLevelName[]
DontSaveSensitive
// end::PackageProtectionLevelName[]

== PackageVersionGUID

// tag::PackageVersionGUID[]
66BF1536-F741-4F07-B417-CE0F5E7827CE
// end::PackageVersionGUID[]

== ProjectPath

// tag::ProjectPath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics
// end::ProjectPath[]

