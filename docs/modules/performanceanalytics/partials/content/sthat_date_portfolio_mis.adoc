// tag::HeaderFullDisplayName[]
= sthAT_Date_Portfolio_MIS.dtsx
// end::HeaderFullDisplayName[]

== GeneralList

// tag::GeneralList[]
[cols="1,4l"]
|===

|PackageCreationDate
|2020-07-29 17:15:52

|PackageCreatorComputerName
|ACPROD

|PackageCreatorName
|ACPROD\dsorokin

|PackageDTSID
|644CDDDE-D24B-48BC-A334-405C4D2EF4DA

|PackageLastModifiedProductVersion
|13.0.4604.0

|PackageLocaleID
|1031

|PackageProtectionLevel
|0

|PackageProtectionLevelName
|DontSaveSensitive

|PackageVersionGUID
|5E8FF099-5CF1-43F6-8D9B-A788C2C9C355

|ProjectPath
|D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics

|===

// end::GeneralList[]

== PumlPackageControlFlows

// tag::PumlPackageControlFlows[]
[plantuml, ssis-{docname}, svg, subs=macros]
....
@startuml

package "sthAT_Date_Portfolio_MIS.dtsx" {
  component "Run procedure CFG_HIST_sthAT_Date_Portfolio_MIS" as packagebackslashrunblankprocedureblankcfgunderlinehistunderlinesthatunderlinedateunderlineportfoliounderlinemis <<Microsoft.ExecuteSQLTask>>  [[../sthat_date_portfolio_mis.html#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlinesthatunderlinedateunderlineportfoliounderlinemis]]
}





skinparam component {
    backgroundColor<<Microsoft.ASExecuteDDLTask>>   Aqua
    backgroundColor<<Microsoft.DTSProcessingTask>>  DeepSkyBlue
    backgroundColor<<Microsoft.ExecutePackageTask>> Yellow
    backgroundColor<<Microsoft.ExecuteSQLTask>>     Khaki
    backgroundColor<<Microsoft.ExpressionTask>>     Pink
    backgroundColor<<Microsoft.Pipeline>>           LightCyan
    backgroundColor<<Microsoft.ScriptTask>>         LightGreen
    backgroundColor<<STOCK:FOREACHLOOP>>            LightGray
    backgroundColor<<STOCK:SEQUENCE>>               White
}



footer The diagram is interactive and contains links.

@enduml

....
// end::PumlPackageControlFlows[]

== ConnectionList

// tag::ConnectionList[]
[#connection-config]
=== config

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|0C7630C8-5571-4C7F-966E-FAC1A78F90AF

|ExpressionValue
|@[$Project::Config]

|===

[#connection-target]
=== Target

[cols="1,4l"]
|===

|ConnectionManagerType
|OLEDB

|ConnectionManagerID
|0EDB69F8-3D1F-4D13-9117-0249E9C85FDC

|ExpressionValue
|@[$Project::Target]

|===

// end::ConnectionList[]

== ProjectConnectionList

// tag::ProjectConnectionList[]

// end::ProjectConnectionList[]

== ParameterList

// tag::ParameterList[]

// end::ParameterList[]

== VariableList

// tag::VariableList[]

// end::VariableList[]

== TaskList

// tag::TaskList[]
[#task-packagebackslashrunblankprocedureblankcfgunderlinehistunderlinesthatunderlinedateunderlineportfoliounderlinemis]
=== Package{backslash}Run procedure CFG_HIST_sthAT_Date_Portfolio_MIS

[cols="1,4l"]
|===

|Task Path
|Package\Run procedure CFG_HIST_sthAT_Date_Portfolio_MIS

|Task Name
|Run procedure CFG_HIST_sthAT_Date_Portfolio_MIS

|Task Type
|Microsoft.ExecuteSQLTask

|Is Disabled
|False

|Sql Connection
|0EDB69F8-3D1F-4D13-9117-0249E9C85FDC

|Sql Package Connection
a| <<connection-target>>

|Sql Statement
a|[%collapsible]
=======
[source,sql,numbered]
----
EXEC [CFG].[HIST_sthAT_Date_Portfolio_MIS]
----
=======


|===

// end::TaskList[]

== PackageDTSID

// tag::PackageDTSID[]
644CDDDE-D24B-48BC-A334-405C4D2EF4DA
// end::PackageDTSID[]

== PackageCreationDate

// tag::PackageCreationDate[]
Jul 29 2020  5:15PM
// end::PackageCreationDate[]

== PackageCreatorComputerName

// tag::PackageCreatorComputerName[]
ACPROD
// end::PackageCreatorComputerName[]

== PackageCreatorName

// tag::PackageCreatorName[]
ACPROD\dsorokin
// end::PackageCreatorName[]

== PackageDescription

// tag::PackageDescription[]

// end::PackageDescription[]

== PackageLastModifiedProductVersion

// tag::PackageLastModifiedProductVersion[]
13.0.4604.0
// end::PackageLastModifiedProductVersion[]

== PackageLocaleID

// tag::PackageLocaleID[]
1031
// end::PackageLocaleID[]

== PackageObjectName

// tag::PackageObjectName[]
sthAT_Date_Portfolio_MIS
// end::PackageObjectName[]

== PackagePath

// tag::PackagePath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics\sthAT_Date_Portfolio_MIS.dtsx
// end::PackagePath[]

== PackageProtectionLevel

// tag::PackageProtectionLevel[]
0
// end::PackageProtectionLevel[]

== PackageProtectionLevelName

// tag::PackageProtectionLevelName[]
DontSaveSensitive
// end::PackageProtectionLevelName[]

== PackageVersionGUID

// tag::PackageVersionGUID[]
5E8FF099-5CF1-43F6-8D9B-A788C2C9C355
// end::PackageVersionGUID[]

== ProjectPath

// tag::ProjectPath[]
D:\Repos\Azure\aisberg\PerformanceAnalytics\SSIS\SSIS_PerformanceAnalytics
// end::ProjectPath[]

